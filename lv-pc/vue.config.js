const os = require('os');

const config = {
  outputDir: '../static/pc',
  publicPath: '/',
  lintOnSave: 'error',
  assetsDir: 'assets',
  productionSourceMap: false,
  devServer: {
    host: '0.0.0.0',
    open: false,
    port: 8080,
    proxy: {
      '/api': {
        target: 'https://mall.duofriend.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
    disableHostCheck: true,
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
  },
};

module.exports = config;
