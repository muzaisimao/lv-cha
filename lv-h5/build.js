#!/usr/bin/env node

const shell = require('shelljs');
const { exec } = require('child_process')
const execa = require('execa')

function exec0() {
    console.log('exec :>>>>>>>>>>>>>>>>>>>>>>>>>> ');
    const child = exec('yarn build', { stdio: 'pipe', shell: true })

    child.stdout.on('data', (data) => {
      console.log( data);
    });

    child.stderr.on('data', (error) => {
      console.log(error);
    });

    child.on('exit', (code) => {
      console.log('exit :>> ', code);
    });

    child.on('error', (error) => {
      console.log('error :>> ', error);
    });
}

function exec1() {
  console.log('shell :>>>>>>>>>>>>>>>>>>>>>>>>>> ');
  const child = shell.exec('yarn build', { async: true, silent: true });
  child.stdout.on('data', (data) => {
    console.log( data);
  });

  child.stderr.on('data', (error) => {
    console.log(error);
  });

  child.on('exit', (code) => {
    console.log('exit :>> ', code);
  });

  child.on('error', (error) => {
    console.log('error :>> ', error);
  });
}

async function exec2() {
  // execa('echo', ['unicorns']).stdout.pipe(process.stdout);
  const child = execa.command('yarn build', { all: true, encoding: 'utf8' });
  child.all.on('data', (data) => {
    console.log(data);
  });
}

exec2()
